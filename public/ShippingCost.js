class ShippingCost {
    static calculate(Province, ntotalWeight, nstandardWeight = .75) {
        let totalCost = 0;
        switch (Province) {
            case "AB":
                totalCost = 53.32 + ((ntotalWeight - nstandardWeight) * 5);
                break;
            case "SK":
                totalCost = 45.44 + ((ntotalWeight - nstandardWeight) * 4.5);
                break;
            case "MB":
                totalCost = 34.26 + ((ntotalWeight - nstandardWeight) * 3.5);
                break;
            case "ON":
                totalCost = 23.19 + ((ntotalWeight - nstandardWeight) * 0.8);
                break;
            case "QC":
                totalCost = 28.37 + ((ntotalWeight - nstandardWeight) * 1.5);
                break;
            case "NS":
                totalCost = 34.26 + ((ntotalWeight - nstandardWeight) * 3.5);
                break;
            case "PE":
                totalCost = 44.11 + ((ntotalWeight - nstandardWeight) * 3.1);
                break;
            default:
                throw "Invalid province";
        }
        return Number(totalCost.toFixed(2));
    }
}
module.exports = ShippingCost;