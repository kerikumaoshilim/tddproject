import Framework7 from 'framework7/framework7.esm.bundle'
import $$ from 'dom7';
const ShippingCost = require('./ShippingCost.js');

const app = new Framework7({
    root: '#app',
    theme: 'auto',
    // Fix for iPhone X notch
    statusbar: {
        overlay: Framework7.device.ios ? Framework7.device.webView || Framework7.device.cordova : 'auto',
    },
});
let weight = 0;
let starndardweight = .75;
$$("#weight").on("keydown", function (e) {
    if (e.keyCode == 13 || e.which == 13) {
        e.preventDefault();
        document.getElementById("province").focus();
       
    }

});
$$("#province").on('keydown', evt => {
    if (evt.keyCode === 13 || evt.which == 13) {
        evt.preventDefault();
        weight = document.getElementById('weight').value
        weight = Number(weight);
        const province = document.getElementById('province').value;
        const totalCost = ShippingCost.calculate(province, weight, starndardweight);
        $$('#totalcost').prepend("<p>" + "Your Shipping cost is: " + totalCost + "</p>")
    }   
});

$$("#shipping").on('submit', evt => {
        evt.preventDefault();
        weight = document.getElementById('weight').value
        weight = Number(weight);
        const province = document.getElementById('province').value;
        const totalCost = ShippingCost.calculate(province, weight, starndardweight);
        $$('#totalcost').prepend("<p>" + "Your Shipping cost is: " + totalCost + "</p>")
    
});

