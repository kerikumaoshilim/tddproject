const SC = require('../public/ShippingCost');

it("tests that Alberta shipping cost is correct", ()=>{
    expect(SC.calculate("AB", 10, .75)).toBe(99.57);
});
it("tests that Saskatchewan shipping cost is correct", ()=>{
    expect(SC.calculate("SK", 10, .75)).toBe(87.06);
});
it("tests that Manitoba shipping cost is correct", ()=>{
    expect(SC.calculate("MB", 10, .75)).toBe(66.63);
});
it("tests that Ontario shipping cost is correct", ()=>{
    expect(SC.calculate("ON", 10, .75)).toBe(30.59);
});
it("tests that Quebec shipping cost is correct", ()=>{
    expect(SC.calculate("QC", 10, .75)).toBe(42.25);
});
it("tests that Ns shipping cost is correct", ()=>{
    expect(SC.calculate("NS", 10, .75)).toBe(66.63);
});
it("tests that Prince Edwards Island shipping cost is correct", ()=>{
    expect(SC.calculate("PE", 10, .75)).toBe(72.78);
});
it("tests that the province \"asdf\" throws an exception", ()=>{
    try{
        SC.calculate("asdf", 10, .75);
        fail();
    }
    catch(e){
        expect(e.toString()).toBe("invalid province");
    }
});